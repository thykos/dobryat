$(document).ready(function() {
  $(window).scroll(function() {
    var elem = $('.navbar'),
      top = $(this).scrollTop();
    if (top > 90) {
      elem.addClass('onscrolling');
    } else {
      elem.removeClass('onscrolling');
    }
  });
  $(".navbar-nav li, .navbar-header").on("click",'.menu-js', function (event) {
    $('.navbar-nav li').removeClass('active');
    $(this).parent().addClass('active');

    event.preventDefault();
    var id = $(this).attr('href'),
      top = $(id).offset().top - 50;
    $('body,html').animate({scrollTop: top}, 500);
  });

  $('.best-js').on('click', function() {
    $('img.img-core, img.penta').each(function() {
      if ($(this).attr("src").indexOf("active") > 0) {
        ($(this).attr("src", $(this).attr("src").replace('/active', '')));
      }
    });
    $(this).find('img.img-core, img.penta').each(function() {
      var src = $(this).attr("src");
      if (src.indexOf("active") < 0) {
        $(this).attr("src", src.replace('blue', 'blue/active'));
      } else {
        ($(this).attr("src", src.replace('/active', '')));
      }
    });
    $('.blue-section .content').addClass('hidden');
    var content = $(this).data("target");
    $('.blue-section').find('.' + content).removeClass('hidden');
  });

  $('#imagePreviewModal').on('show.bs.modal', function(event) {
    var button = $(event.relatedTarget),
      imageSrc = button.data('image'),
      modal = $(this);
    modal.find('.target-image').attr("src", imageSrc);
  });

  $('.d-tab').on('click', function() {
    var tab = $(this).data("target"),
      tabs = $('.tabs-section');
    tabs.find('.content .container').removeClass('seo audit men').addClass(tab);
    tabs.find('.content .container .d-title').addClass('hidden');
    tabs.find('.content .container .d-title.' + tab).removeClass('hidden');
    tabs.find('.content .container .content-text').addClass('hidden');
    tabs.find('.content .container .content-text.' + tab).removeClass('hidden');
  });

  $('.slider').slick({
    centerMode: true,
    centerPadding: '60px',
    variableWidth: false,
    slidesToShow: 3,
    arrows: false,
    focusOnSelect: true,
    responsive: [
      {
        breakpoint: 992,
        settings: {
          arrows: false,
          centerMode: true,
          centerPadding: '40px',
          slidesToShow: 1
        }
      }
    ]
  });
});
